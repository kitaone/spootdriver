package com.spoot.spootdriver;

/**
 * Created by Kita on 01.06.2018.
 */

public class Ride {

    private StopInformation stopFrom;
    private StopInformation stopTo;

    public void setStopFrom(StopInformation stopFrom){
        this.stopFrom = stopFrom;
    }

    public StopInformation getStopFrom(){
        return stopFrom;
    }

    public void setStopTo(StopInformation stopTo){
        this.stopTo = stopTo;
    }

    public StopInformation getStopTo(){
        return stopTo;
    }
}
