package com.spoot.spootdriver;

import android.app.ListActivity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    LocationManager locationManager;
    double longitude, latitude;

    public int notifCount = 2;

    ProgressDialog progress;

    public static ListView route;
    public static List<Ride> rides;
    public static List<StopInformation> stops;

    public Context context = this;

    public static NotificationManager manager;

    GoogleMap mGoogleMap;
    MapView mMapView;

    //public String serverAddres = getResources().getString(R.string.spoot_server_local);
    public static String serverAddres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        serverAddres = getResources().getString(R.string.spoot_server_local);

        mMapView = (MapView) findViewById(R.id.mapView);
        if(mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

        route = (ListView) findViewById(R.id.route);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.firstLine);
        route.setAdapter(adapter);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        toggleNetworkUpdates();
        addNotification();

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                try{
                    //do your code here
                    new GetUrlContentTask().execute(serverAddres + "getpendingrides");
                }
                catch (Exception e) {
                }
                finally{
                    //also call the same runnable to call it at regular interval
                    handler.postDelayed(this, 10000);
                }
            }
        };
        runnable.run();
    }

    private void addNotification() {
        android.support.v4.app.NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Spoot Driver")
                        .setContentText("Przejdź do aplikacji i rozpocznij prace.");

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }



    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext.setQueryRateLimit(3).setApiKey("AIzaSyBiUUsWxQM_mZ6Qzelj7kaYJYC1d2rj8YA").setConnectTimeout(1, TimeUnit.SECONDS).setReadTimeout(1, TimeUnit.SECONDS).setWriteTimeout(1, TimeUnit.SECONDS);
    }

    private void drawRoute(){
        DateTime now = new DateTime();
        if(stops.get(0) != null){
            try {
                mGoogleMap.clear();
                //LatLng source = new LatLng(mGoogleMap.getMyLocation().getLatitude(), mGoogleMap.getMyLocation().getLongitude());
                LatLng source = new LatLng(latitude, longitude);
                LatLng firstDest = new LatLng(stops.get(0).getLatitude(), stops.get(0).getLongitude());
                DirectionsResult firstRresult = DirectionsApi.newRequest(getGeoContext()).mode(TravelMode.DRIVING).origin(source).destination(firstDest).departureTime(now).await();
                addPolyline(firstRresult);
            } catch (ApiException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        for(int i = 0; i < stops.size()-1 ; i++){
            if(stops.get(i+1) != null){
                try {
                    StopInformation stop = stops.get(i);
                    StopInformation next = stops.get(i+1);
                    LatLng origin = new LatLng(stop.getLatitude(), stop.getLongitude());
                    LatLng destination = new LatLng(next.getLatitude(), next.getLongitude());
                    com.google.android.gms.maps.model.LatLng origin1 = new com.google.android.gms.maps.model.LatLng(stop.getLatitude(), stop.getLongitude());
                    com.google.android.gms.maps.model.LatLng dest1 = new com.google.android.gms.maps.model.LatLng(next.getLatitude(), next.getLongitude());
                    mGoogleMap.addMarker(new MarkerOptions().position(origin1).title(stops.get(i).getName()));
                    mGoogleMap.addMarker(new MarkerOptions().position(dest1).title(stops.get(i+1).getName()));
                    DirectionsResult result = DirectionsApi.newRequest(getGeoContext()).mode(TravelMode.DRIVING).origin(origin).destination(destination).departureTime(now).await();
                    addPolyline(result);
                } catch (ApiException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void addPolyline(DirectionsResult results) {
        List<com.google.android.gms.maps.model.LatLng> decodedPath = PolyUtil.decode(results.routes[0].overviewPolyline.getEncodedPath());
        mGoogleMap.addPolyline(new PolylineOptions().addAll(decodedPath));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this);
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    public void toggleNetworkUpdates() {
        if (!checkLocation())
            return;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60 * 1000, 10, locationListenerNetwork);
        Toast.makeText(this, "Network provider started running", Toast.LENGTH_LONG).show();
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private final LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            final com.google.android.gms.maps.model.LatLng myLoc = new com.google.android.gms.maps.model.LatLng(latitude, longitude);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Network Provider update", Toast.LENGTH_SHORT).show();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myLoc, 15);
                    mGoogleMap.animateCamera(cameraUpdate);
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void startWork(View w){
        new GetUrlContentTask().execute(serverAddres + "waittostart/100/" + latitude + "/" + longitude);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.startWorkMenu) {
            manager.cancel(0);
            android.support.v4.app.NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("Spoot Driver")
                            .setContentText("Praca rozpoczęta.");

            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(contentIntent);

            manager.notify(1, builder.build());

            new GetUrlContentTask().execute(serverAddres + "waittostart/100/" + latitude + "/" + longitude);
            return true;
        }else if(id == R.id.downloadRouteMenu){
            progress = ProgressDialog.show(this, "Pobieranie", "Pobieranie trasy", true);
            new GetUrlContentTask().execute(serverAddres + "getstops");
            return true;
        } else if(id == R.id.server_gcloud){
            if(item.isChecked()){
                serverAddres = getResources().getString(R.string.spoot_server_local);
                item.setChecked(false);
            }else{
                serverAddres = getResources().getString(R.string.spoot_server_gcloud);
                item.setChecked(true);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void setStops(){
        RouteAdapter adapter = new RouteAdapter(this, R.layout.list_item, (ArrayList<StopInformation>) stops);
        route.setAdapter(adapter);
    }

    private class GetUrlContentTask extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            String content = "";
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                content = "";
                String line;
                while ((line = rd.readLine()) != null) {
                    content += line + "\n";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return content;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            //TextView textRequest = (TextView) findViewById(R.id.textRequest);
            JSONObject obj = null;
            try{
                obj = new JSONObject(result);
                if(obj.get("info").equals("stops")){
                    JSONArray content = obj.getJSONArray("content");
                    List<StopInformation> stopList = new ArrayList<StopInformation>(content.length());
                    for(int i = 0; i < content.length(); i++){
                        JSONObject stopJ = (JSONObject) content.get(i);
                        StopInformation stop = new StopInformation();
                        stop.setDelivery(stopJ.getBoolean("delivery"));
                        stop.setLatitude(stopJ.getDouble("latitude"));
                        stop.setLongitude(stopJ.getDouble("longtitude"));
                        stop.setName(stopJ.getString("name"));
                        stop.setAdditionalLocation(stopJ.getString("additionalLocation"));
                        stop.setAddress(stopJ.getString("address"));
                        stopList.add(stop);
                    }
                    stops = stopList;
                    progress.dismiss();
                    setStops();
                    drawRoute();

                    //Toast.makeText(MainActivity.this, obj.get(0).toString(), Toast.LENGTH_SHORT).show();
                } else if(obj.get("info").equals("pendingRides")){
                    JSONArray content = obj.getJSONArray("content");
                    if(content.length() > 0){
                        JSONObject info = (JSONObject) content.get(0);
                        addPendingNotification(info);
                    }
                    if(obj.getBoolean("routeReady")){
                        progress = ProgressDialog.show(context, "Pobieranie", "Pobieranie trasy", true);
                        new GetUrlContentTask().execute(serverAddres + "getstops");
                    }
/*                    for(int i = 0 ; i < content.length() ; i++){
                        JSONObject info = (JSONObject) content.get(i);
                        addPendingNotification(info);
                    }*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Intent getNotificationIntent(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public void addPendingNotification(JSONObject info){
        try{
            Intent approveIntent = getNotificationIntent();
            approveIntent.setAction("APPROVE");
            approveIntent.putExtra("rideName", info.getString("rideName"));
            approveIntent.putExtra("rideNumber", info.getString("rideNumber"));

            Intent cancelIntent = getNotificationIntent();
            cancelIntent.setAction("CANCEL");
            cancelIntent.putExtra("rideName", info.getString("rideName"));
            cancelIntent.putExtra("rideNumber", info.getString("rideNumber"));

            android.support.v4.app.NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("Nowy przewóz")
                            .setContentText("Z lokalizacji " + info.getString("pickupAddress") + " do lokalizacji " + info.getString("deliveryAddress"))
                            .setStyle(new NotificationCompat.BigTextStyle().bigText("Z lokalizacji " + info.getString("pickupAddress") + " do lokalizacji " + info.getString("deliveryAddress")))
                            .addAction(new android.support.v4.app.NotificationCompat.Action(R.mipmap.ic_launcher, "Tak", PendingIntent.getActivity(this, 0, approveIntent, PendingIntent.FLAG_UPDATE_CURRENT)))
                            .addAction(new android.support.v4.app.NotificationCompat.Action(R.mipmap.ic_launcher, "Nie", PendingIntent.getActivity(this, 0, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT)));

            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(contentIntent);

            // Add as notification
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(2, builder.build());
        } catch(JSONException e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onNewIntent(Intent intent){
        processIntenetAction(intent);
        super.onNewIntent(intent);
    }

    private void processIntenetAction(Intent intent){
        if(intent.getAction() != null){
            switch (intent.getAction()){
                case "APPROVE":
                    //manager.cancel(intent.getIntExtra("rideNumber", 3));
                    manager.cancel(2);
                    Toast.makeText(this, "Approve" + intent.getStringExtra("rideName"), Toast.LENGTH_LONG).show();
                    //progress = ProgressDialog.show(this, "Pobieranie", "Pobieranie trasy", true);
                    new GetUrlContentTask().execute(serverAddres + "approveride/" + intent.getStringExtra("rideName"));
/*                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            new GetUrlContentTask().execute(serverAddres + "getstops");
                        }
                    }, 5000);*/
                    break;
                case "CANCEL":
                    //manager.cancel(intent.getIntExtra("rideNumber", 3));
                    manager.cancel(2);
                    new GetUrlContentTask().execute(serverAddres + "cancelride/" + intent.getStringExtra("rideName"));
                    Toast.makeText(this, "Cancel" + intent.getStringExtra("rideName"), Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
}
