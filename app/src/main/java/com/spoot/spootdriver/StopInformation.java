package com.spoot.spootdriver;

/**
 * Created by Kita on 01.06.2018.
 */

public class StopInformation {

    private String name;
    private boolean delivery;
    private double longitude;
    private double latitude;
    private String additionalLocation;
    private String address;

    public String getAddress(){
        return address;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public String getAdditionalLocation() {
        return additionalLocation;
    }

    public void setAdditionalLocation(String additionalLocation) {
        this.additionalLocation = additionalLocation;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }



}
