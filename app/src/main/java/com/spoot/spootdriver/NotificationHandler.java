package com.spoot.spootdriver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class NotificationHandler extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_handler);
    }

    @Override
    protected void onNewIntent(Intent intent){
        processIntenetAction(intent);
        super.onNewIntent(intent);
    }

    private void processIntenetAction(Intent intent){
        if(intent.getAction() != null){
            switch (intent.getAction()){
                case "APPROVE":
                    MainActivity.manager.cancel(intent.getIntExtra("rideNumber", 3));
                    Toast.makeText(this, "Approve" + intent.getStringExtra("rideName"), Toast.LENGTH_LONG).show();
                    //new GetUrlContentTask().execute("http://192.168.0.106:8080/spoot/spoot/approveride/" + intent.getStringExtra("rideName"));
                    break;
                case "CANCEL":
                    MainActivity.manager.cancel(intent.getIntExtra("rideNumber", 3));
                    Toast.makeText(this, "Cancel" + intent.getStringExtra("rideName"), Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }
}
