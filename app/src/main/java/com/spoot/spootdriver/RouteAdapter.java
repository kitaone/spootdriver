package com.spoot.spootdriver;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kita on 01.06.2018.
 */

public class RouteAdapter extends ArrayAdapter<StopInformation> {

    private final Context context;
    private final ArrayList<StopInformation> stops;

    public RouteAdapter(Context context, int resource, ArrayList<StopInformation> items) {
        super(context, resource, items);
        this.context = context;
        this.stops = items;
    }

    @Override
    public View getView(final int position, View covertView, final ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, parent, false);
        TextView firstLine = (TextView) rowView.findViewById(R.id.firstLine);
        firstLine.setText(stops.get(position).getAddress());

        TextView secondLine = (TextView) rowView.findViewById(R.id.secondLine);
        //secondLine.setText(!stops.get(position).isDelivery() ? "Lokalizacja odbioru | " + stops.get(position).getName() + "->" + stops.get(position).getAdditionalLocation() : "Lokalizacja dostarczenia | " + stops.get(position).getAdditionalLocation() + "->" + stops.get(position).getName());
        secondLine.setText((!stops.get(position).isDelivery() ? "Lokalizacja odbioru" : "Lokalizacja dostarczenia") + " | Identyfikator " + stops.get(position).getName().substring(0, stops.get(position).getName().length()-3));

        Button navigateTo = (Button) rowView.findViewById(R.id.navigateTo);
        Button cancel = (Button) rowView.findViewById(R.id.cancel);

        navigateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.manager.cancelAll();
                android.support.v4.app.NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(context)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle("Spoot Driver")
                                .setContentText("Nawigacja do " + stops.get(position).getName());

                Intent notificationIntent = new Intent(context, MainActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                RelativeLayout layout = (RelativeLayout) v.getParent();
                layout.setBackgroundColor(Color.GREEN);
                new GetUrlContentTask().execute(MainActivity.serverAddres + "locklocation/" + stops.get(position).getName());
                MainActivity.manager.notify(2, builder.build());
                //Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=" + stops.get(position).getLatitude() + "," + stops.get(position).getLongitude()));
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + stops.get(position).getLatitude() + "," + stops.get(position).getLongitude()));
                context.startActivity(intent);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new GetUrlContentTask().execute(MainActivity.serverAddres + "locklocation/" + stops.get(position).getName());
                new GetUrlContentTask().execute(MainActivity.serverAddres + "hidelocation/" + stops.get(position).getName());
                stops.remove(stops.get(position));
                notifyDataSetChanged();
            }
        });

        return rowView;
    }

    private class GetUrlContentTask extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            String content = "";
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                content = "";
                String line;
                while ((line = rd.readLine()) != null) {
                    content += line + "\n";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return content;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            //TextView textRequest = (TextView) findViewById(R.id.textRequest);
            JSONObject obj = null;
            try{
                obj = new JSONObject(result);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
